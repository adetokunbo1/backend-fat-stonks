const fastify = require('fastify')({
  logger: true,
});
const proxy = require('fastify-http-proxy');
const mongoose = require('mongoose');
require('dotenv').config();
const boom = require('@hapi/boom');
const routes = require('./routes/index');
const User = require('./models/user');
const Stock = require('./models/stock');
const fetch = require('node-fetch');
const KEY = require('./config/keys').SECRET;
const MONGO_URL = require('./config/keys').MONGO_URI;
const EXTERNAL_API = require('./config/keys').EXTERNAL_API;
const PORT = require('./config/keys').PORT;
const API_KEY = require('./config/keys').API_KEY;
const ENV = require('./config/keys').NODE_ENV
const SERVER_VERSION = require('./config/keys').SERVER_VERSION;
const helmet = require('fastify-helmet');
const { fastifySchedulePlugin } = require('fastify-schedule');
const { SimpleIntervalJob, AsyncTask } = require('toad-scheduler');


/*******************************/
/****** Fastify Plugins *******/
/*****************************/
fastify.register(require('fastify-jwt'), {
  secret: KEY,
  cookie: {
    cookieName: 'token',
  },
});
fastify.register(helmet);
fastify.register(require('fastify-cookie'));
fastify.register(require('fastify-websocket'), {
  errorHandler: async function (
    error,
    conn /* SocketStream */,
    req /* FastifyRequest */,
    reply /* FastifyReply */
  ) {
    if (error) {
      conn.destroy(error);
    }
  },
  options: {
    maxPayload: 1048576,
  },
});
fastify.register(fastifySchedulePlugin);
if(ENV === 'production') {
  fastify.register(require('fastify-cors'), {
    origin: [/^https?:\/\/fatstonks.org\/?[a-z]*.*/, 'https://fatstonks.org'],
    credentials: true,
    optionsSuccessStatus: 200,
  });
} else {
  fastify.register(require('fastify-cors'), {
    origin: [/^http?:\/\/localhost:8080\/?[a-z]*.*/, 'http://localhost:8080'],
    credentials: true,
    optionsSuccessStatus: 200,
});
}

/********************************/
/****** Global Variables *******/
/******************************/
let WEBSOCKET_CLIENTS = [];

/***********************/
/****** MongoDB *******/
/*********************/
mongoose
  .connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(async () => {
    console.log('MongoDB connected...');
    await fetchDefaultStocks();
    retrieveGeneralStocksJob();
  })
  .catch((err) => console.log(err));

/*********************/
/****** Proxy *******/
/*******************/
fastify.register(proxy, {
  upstream: EXTERNAL_API,
  prefix: '/api/stocks',
  undici: true,
  preHandler: async (req, reply) => {
    try {
      await req.jwtVerify();
      //Somewhat ugly solution to adding api-key. Altering req.query does not work, weirdly.
      req.raw.url = `${req.raw.url}&apikey=${API_KEY}`;
    } catch (err) {
      boom.boomify(err);
    }
  },
  replyOptions: {
    //TODO: handle response from twelvedata here, save to database or whatever
    // onResponse: (request, reply, res) => {
    //   reply.send(res);
    // },
  },
});

/*********************/
/****** Hooks *******/
/*******************/
fastify.addHook('onSend', async (request, reply, payload) => {
  let date = new Date();
  let todaysDateLocale = date.toLocaleString('se-SE');
  if(ENV === 'production'){
  await reply.headers({
    ...reply.headers,
    Date: todaysDateLocale,
    'Access-Control-Allow-Origin': 'https://fatstonks.org',
    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE, PATCH',
    'Access-Control-Allow-Headers': 'Origin, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
    'Content-Type': 'application/json'
  });
  } else {
    await reply.headers({
      ...reply.headers,
      Date: todaysDateLocale,
      'Access-Control-Allow-Origin': 'http://localhost:8080',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE, PATCH',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
      'Content-Type': 'application/json'
    });
  }
});

/**********************/
/****** Routes *******/
/********************/
fastify.get('/', (request, reply) => {
  reply.send(`version ${SERVER_VERSION}`)
})

routes.noAuthRoutes.forEach((route, index) => {
  fastify.route(route);
});

/************************************/
/****** Authenticated Routes *******/
/**********************************/
routes.authenticatedRoutes.forEach((route, index) => {
  fastify.route(route);
});

fastify.get(
  '/api/ws/register',
  { websocket: true },
  async (connection /* SocketStream */, req /* FastifyRequest */) => {
    const socketID = req.headers['sec-websocket-key'];
    try {
      let decodedClientToken = await req.jwtVerify();
      fastify.log.info({
        msg: 'Websocket client connected',
        socket: socketID,
      });
      const websocketClientInfo = {
        ...decodedClientToken,
        socket: socketID,
        active: true,
      };
      WEBSOCKET_CLIENTS.push(websocketClientInfo);
      fetchUserStocks(connection, websocketClientInfo);
      pollForUserWatchedStocks(connection, websocketClientInfo);
    } catch (err) {
      const error = boom.boomify(err);
      connection.socket.send(error.message);
      if (WEBSOCKET_CLIENTS) {
        WEBSOCKET_CLIENTS = WEBSOCKET_CLIENTS.filter(
          (obj) => obj.socket !== socketID
        );
      }
      connection.socket.terminate();
      fastify.log.info({
        msg: 'Websocket client disconnected',
        socket: socketID,
      });
      return error;
    }
  }
);

/***********************************/
/****** Toad-scheduler Jobs *******/
/*********************************/
async function pollForUserWatchedStocks(connection, wsClientInfo) {
  console.log(WEBSOCKET_CLIENTS)
  console.log('Poll started');
  const pollUserStocksData = new AsyncTask(
    `poll userStocks ${wsClientInfo.socket}`,
    () => { return fetchUserStocks(connection, wsClientInfo)},
    (err) => {
      console.log(err);
      const error = boom.boomify(err);
      connection.socket.send(error.message);
      WEBSOCKET_CLIENTS = WEBSOCKET_CLIENTS.filter(
        (obj) => obj.socket !== wsClientInfo.socket
      );
      fastify.scheduler.stopById(`Update_UserData_${wsClientInfo.socket}`);
      connection.socket.close(error.statusCode, error.message);
      fastify.log.info({
        msg: 'Websocket client disconnected',
        socket: wsClientInfo.socket,
      });
      return error;
    }
  );
  const job = new SimpleIntervalJob(
    { seconds: 60 },
    pollUserStocksData,
    `Update_UserData_${wsClientInfo.socket}`
  );
  fastify.scheduler.addSimpleIntervalJob(job);
}

async function retrieveGeneralStocksJob() {
  console.log('Starting general stock polling');
  const pollUserStocksData = new AsyncTask(
    `poll defaultstocks`,
    () => {
      return fetchDefaultStocks();
    },
    (err) => {
      const error = boom.boomify(err);
      return error;
    }
  );
  const job = new SimpleIntervalJob(
    //perform job every hour
    { seconds: 3600 },
    pollUserStocksData,
    `Update_DefaultStocks`
  );
  fastify.scheduler.addSimpleIntervalJob(job);
}

/*************************/
/****** Functions *******/
/***********************/
async function fetchUserStocks(connection, wsClientInfo) {
  //TODO: handle this here? it tracks updated stock data (if user removes/adds a stock)
  //drawback is alot of database queries...
  const databaseUser = await User.findById(wsClientInfo._id);
  const watchedStocks = databaseUser.watched_stocks;
  let symbols = '';
  if (watchedStocks.length === 0) {
    return false;
  }
  for (let i = 0; i < watchedStocks.length; i++) {
    if (i == watchedStocks.length - 1) {
      symbols += `${watchedStocks[i].symbol}`;
    } else {
      symbols += `${watchedStocks[i].symbol},`;
    }
  }
  // const url = `${EXTERNAL_API}/time_series?symbol=${symbols}&apikey=${API_KEY}`;
  // const url = `${EXTERNAL_API}/quote?symbol=${symbols}&apikey=${API_KEY}`;
  const url = `${EXTERNAL_API}/price?symbol=${symbols}&apikey=${API_KEY}`;
  const options = {
    method: 'GET',
  };
    return fetch(url, options)
      .then((response) => response.json())
      .then((data) => {
        if (connection.socket.readyState !== 1) {
          throw new Error('Client not connected');
        } else {
          connection.socket.send(JSON.stringify(data));
        }
      })
}

async function fetchDefaultStocks() {
  await Stock.deleteMany();
  console.log('Stock data emptied, ready to update');
  const symbols = [
    'TSLA',
    'AMZN',
    'FB',
    'MSFT',
    'NVDA',
    'BABA',
    'AMD',
    'GOOGL',
    'GOOG',
    'CSCO',
    'ATA',
    'COIN',
    'AMAT',
    'BA',
    'PYPL',
    'SHOP',
    'NFLX',
    'SPCE',
    'SQ',
    'V',
    'JPMJL',
    'TGT',
    'BAC',
    'SEAH',
    'VIAC',
    'MA',
    'CRM',
    'SNOW',
    'WMT',
    'MU',
    'SNAP',
    'JD',
    'F',
    'INTC',
    'DIS',
    'LRCX',
    'ROKU',
    'QCOM',
    'HD',
    'XOM',
    'ABNB',
    'PG',
    'WFCCL',
    'VZ',
    'PTON',
    'C',
  ];
  let symbolsString = '';
  for (let i = 0; i < symbols.length; i++) {
    if (i === symbols.length - 1) {
      symbolsString += `${symbols[i]}`;
    } else {
      symbolsString += `${symbols[i]},`;
    }
  }
  const url = `${EXTERNAL_API}/time_series?symbol=${symbolsString}&interval=1day&outputsize=1095&apikey=${API_KEY}`;
  const options = {
    method: 'GET',
  };
  try {
    await fetch(url, options)
      .then(function (response) {
        if (response.status !== 200) {
          fastify.log.info({
            msg: 'Error fetching defaultstocks to store in database',
          });
          return false;
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        Object.entries(data).forEach((obj) => {
          if(obj[1].status == 'error') {
            delete obj;
          } else {
          let restructure = {
            symbol: obj[0],
            ...obj[1],
          };
          let databaseStock = new Stock(restructure);
          databaseStock.save();
        }
        });
      });
  } catch (err) {
    console.log(err);
  }
}

/**********************/
/****** Server *******/
/********************/
const start = async () => {
  try {
    await fastify.listen(PORT || 3000, '0.0.0.0');
    console.log(`Server started in || ${ENV} || mode`)
    console.log(`Current version || ${SERVER_VERSION} ||`)
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
