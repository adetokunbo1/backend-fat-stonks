FROM node:current-alpine

WORKDIR app/
# Install app dependencies
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
# Bundle app source
COPY . .

ENV SECRET=UltiMatEsEcreT1337L0L
ENV PORT=80
ENV EXTERNAL_STOCK_API=https://api.twelvedata.com
ENV API_KEY=23782c8315b6461ea54b84d1ffd9070b
ENV MONGODB_USERNAME=luktuc
ENV MONGODB_PASSWORD=luktuc1337!
ENV NODE_ENV=production

EXPOSE 80

CMD [ "node", "server.js" ]
