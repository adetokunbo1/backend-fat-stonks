const userController = require('../controllers/userController');

const userStockController = require('../controllers/userStockController');

const generalStocksController = require('../controllers/generalStocksController');

/****************************************/
/****** No Authentication Routes *******/
/**************************************/
exports.noAuthRoutes = [
  {
    method: 'POST',
    url: '/api/users/register',
    handler: userController.registerUser,
  },
  {
    method: 'POST',
    url: '/api/users/login',
    handler: userController.loginUser,
  },
  {
    method: 'GET',
    url: '/stocks',
    handler: generalStocksController.getDefaultStockList,
  },
];

/************************************/
/****** Authenticated Routes *******/
/**********************************/
exports.authenticatedRoutes = [
  {
    method: 'PUT',
    url: '/api/users',
    handler: userController.updateUser,
  },
  {
    method: 'DELETE',
    url: '/api/users',
    handler: userController.deleteUser,
  },
  {
    method: 'GET',
    url: '/api/users',
    handler: userController.findSingleUser,
  },
  {
    method: 'POST',
    url: '/api/users/logout',
    handler: userController.logoutUser,
  },
  {
    method: 'POST',
    url: '/api/users/stocks',
    handler: userStockController.addStockToWatchList,
  },
  {
    method: 'DELETE',
    url: '/api/users/stocks',
    handler: userStockController.removeStockFromWatchList,
  },
  {
    method: 'GET',
    url: '/api/users/stocks',
    handler: userStockController.getWatchList,
  },
  {
    method: 'GET',
    url: '/api/users/auth',
    handler: userController.authenticateUser,
  },
];
