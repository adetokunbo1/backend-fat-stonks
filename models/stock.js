const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/***************************/
/****** Stock Schema *******/
/*************************/

//TODO: default stocks schema vol.1 (can change with time, not sure yet exactly how we want it structured)
const StockSchema = new Schema({
  symbol: {
    type: String,
    required: true,
  },
  meta: {
    symbol: {
      type: String,
      required: true,
    },
    interval: {
      type: String,
      required: true,
    },
    currency: {
      type: String,
      required: true,
    },
    exchange_timezone: {
      type: String,
      required: true,
    },
    exchange: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
  },
  values: [
    {
      _id: false,
      datetime: {
        type: String,
        required: true,
      },
      open: {
        type: Number,
        required: true,
      },
      high: {
        type: Number,
        required: true,
      },
      low: {
        type: Number,
        required: true,
      },
      close: {
        type: Number,
        required: true,
      },
      volume: {
        type: Number,
        required: true,
      },
    },
  ],
});

module.exports = Stock = mongoose.model('stocks', StockSchema);
