const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/***************************/
/****** User Schema *******/
/*************************/

const UserSchema = new Schema({
  forename: {
    type: String,
    required: true,
  },
  surname: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: false,
  },
  email: {
    type: String,
    required: true,
  },
  registration_date: {
    type: String,
    required: true,
  },
  watched_stocks: {
    type: Array,
    required: false,
    stock: {
      //TODO: stocks schema vol.1 (can change with time, not sure yet exactly how we want it structured)
      symbol: {
        type: String,
        required: true,
      },
      same: {
        type: String,
        required: true,
      },
      exchange: {
        type: String,
        required: true,
      },
      currency: {
        type: String,
        required: true,
      },
      datetime: {
        type: String,
        required: true,
      },
      open: {
        type: Number,
        required: true,
      },
      high: {
        type: Number,
        required: true,
      },
      low: {
        type: Number,
        required: true,
      },
      close: {
        type: Number,
        required: true,
      },
      volume: {
        type: Number,
        required: true,
      },
      previous_close: {
        type: Number,
        required: true,
      },
      change: {
        type: Number,
        required: true,
      },
      percent_change: {
        type: Number,
        required: true,
      },
      average_volume: {
        type: Number,
        required: true,
      },
      fifty_two_week: {
        low: {
          type: Number,
          required: true,
        },
        high: {
          type: Number,
          required: true,
        },
        low_Change: {
          type: Number,
          required: true,
        },
        high_Change: {
          type: Number,
          required: true,
        },
        low_Change_Percent: {
          type: Number,
          required: true,
        },
        high_Change_Percent: {
          type: Number,
          required: true,
        },
        range: {
          type: String,
          required: true,
        },
      },
    },
  },
});

module.exports = User = mongoose.model('users', UserSchema);
