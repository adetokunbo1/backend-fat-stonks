### Current version: 1.2.1

run npm install

copy the content of .env.example into a file named .env and fill out the variables

run
`npm run start`
which launches the server in hot reload mode with nodemon

### ROUTES

# /**\*\*** No Authentication Routes **\*\*\***/

`{ method: 'POST', url: '/api/users/register' }`
`{ method: 'POST', url: '/api/users/login' }`
`{ method: 'GET', url: '/stocks' }`

# /**\*\*** Authenticated Routes **\*\*\***/

`{ method: 'PUT', url: '/api/users' } `
`{ method: 'DELETE', url: '/api/users' } `
`{ method: 'GET', url: '/api/users' } `
`{ method: 'POST', url: '/api/users/logout' } `
`{ method: 'GET', url: '/api/users/stocks' } `
`{ method: 'POST', url: '/api/users/stocks' } `
`{ method: 'DELETE', url: '/api/users/stocks' } `
`{ method: 'GET', url: '/api/users/auth' } `

# /**\*\*** Proxied Routes (Authenticated) **\*\*\***/

`{ method: 'GET', url: '/api/stocks/*', }`

# /**\*\*** WebSocket route for connection (Authenticated) **\*\*\***/

`{ method: 'GET', url: '/api/ws/register', }`
