require('dotenv').config();

module.exports = {
  MONGO_URI: `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@fat-stonks.whmvp.mongodb.net/fat-stonks-usersdb?retryWrites=true&w=majority`,
  SECRET: `${process.env.SECRET}`,
  EXTERNAL_API: process.env.EXTERNAL_STOCK_API,
  API_KEY: process.env.API_KEY,
  PORT: process.env.PORT,
  NODE_ENV: process.env.NODE_ENV,
  SERVER_VERSION: process.env.npm_package_version,
};
