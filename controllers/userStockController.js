const boom = require('@hapi/boom');
const User = require('../models/user');
const fetch = require('node-fetch');
const EXTERNAL_API = require('../config/keys').EXTERNAL_API;
const API_KEY = require('../config/keys').API_KEY;

/*****************************************/
/****** Authenticated Controllers *******/
/***************************************/
exports.addStockToWatchList = async (req, reply) => {
  try {
    const decodedClientToken = await req.jwtVerify();
    const userInfo = { ...decodedClientToken };
    const symbol = req.body.symbol;
    const exchange = req.body.exchange;
    const user = await User.findById(userInfo._id);
    const url = `${EXTERNAL_API}/quote?symbol=${symbol}&exchange=${exchange}&apikey=${API_KEY}`;
    if (
      user.watched_stocks.some(
        (e) => e.symbol === symbol && e.exchange === exchange
      )
    ) {
      var error = new Error('This stock already exists in your watchlist');
      error.statusCode = 400;
      throw error;
    }
    let stock;
    await fetch(url)
      .then(function (response) {
        if (response.status !== 200) {
          var error = new Error('Stock not found');
          error.statusCode = response.status;
          throw error;
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          stock = data;
          stock = {
            symbol: stock.symbol,
            name: stock.name,
            exchange: stock.exchange,
            currency: stock.currency,
            datetime: stock.datetime,
            open: Number(stock.open),
            high: Number(stock.high),
            low: Number(stock.low),
            close: Number(stock.close),
            volume: Number(stock.volume),
            previous_close: Number(stock.previous_close),
            change: Number(stock.change),
            percent_change: Number(stock.percent_change),
            average_volume: Number(stock.average_volume),
            fifty_two_week: {
              low: Number(stock.fifty_two_week.low),
              high: Number(stock.fifty_two_week.high),
              low_change: Number(stock.fifty_two_week.low_change),
              high_change: Number(stock.fifty_two_week.high_change),
              low_change_percent: Number(
                stock.fifty_two_week.low_change_percent
              ),
              high_change_percent: Number(
                stock.fifty_two_week.high_change_percent
              ),
              range: stock.fifty_two_week.range,
            },
          };
        }
      })
      .catch((err) => {
        throw boom.boomify(err);
      });
    user.watched_stocks.push(stock);
    await user.validate();
    const update = await User.findByIdAndUpdate(userInfo._id, user, {
      new: true,
    });
    return update.watched_stocks;
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.removeStockFromWatchList = async (req, reply) => {
  try {
    const decodedClientToken = await req.jwtVerify();
    const userInfo = { ...decodedClientToken };
    const symbol = req.body.symbol;
    const exchange = req.body.exchange;
    await User.findByIdAndUpdate(userInfo._id, {
      $pull: { watched_stocks: { symbol: symbol, exchange: exchange } },
    });
    return reply.send(`${symbol} removed from watchlist`);
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.getWatchList = async (req, reply) => {
  try {
    const decodedClientToken = await req.jwtVerify();
    const userInfo = { ...decodedClientToken };
    const user = await User.findById(userInfo._id);
    return user.watched_stocks;
  } catch (err) {
    throw boom.boomify(err);
  }
};
