const boom = require('@hapi/boom');
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const ENV = require('../config/keys').NODE_ENV;

/***********************************/
/****** No Auth Controllers *******/
/*********************************/
exports.registerUser = async (req, reply) => {
  try {
    const user = new User(req.body);
    const userEmail = { email: user.email };
    const possibleUser = await User.findOne(userEmail);
    if (possibleUser) {
      var error = new Error(
        'An account already exists with this e-mail address'
      );
      error.statusCode = 400;
      throw error;
    }
    var salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
    var date = new Date();
    var todaysDateLocale = date.toLocaleString('se-SE');
    user.registration_date = todaysDateLocale;
    await user.save();
    token = await reply.jwtSign({
      _id: user._id,
      forename: user.forename,
      surname: user.surname,
    });
    if (ENV === 'production') {
      return reply
        .setCookie('token', token, {
          secure: true,
          //Expiration date of 1 hour
          maxAge: 60 * 60 * 1,
          domain: 'fatstonks.org',
          path: '/',
          httpOnly: true,
          sameSite: 'none',
        })
        .code(200)
        .send({ url: '/user/portfolio' });
    } else {
      return reply
        .setCookie('token', token, {
          //Expiration date of 1 hour
          maxAge: 60 * 60 * 1,
          domain: 'localhost',
          path: '/',
          httpOnly: true,
          sameSite: true,
        })
        .code(200)
        .send({ url: '/user/portfolio' });
    }
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.loginUser = async (req, reply) => {
  try {
    const userToCheck = req.body;
    const userEmail = {
      email: userToCheck.email,
    };
    const possibleUser = await User.findOne(userEmail);
    if (!possibleUser) {
      var error = new Error('Account not found');
      error.statusCode = 401;
      throw error;
    }
    const checkPassword = bcrypt.compareSync(
      userToCheck.password,
      possibleUser.password
    );
    let token;
    if (checkPassword === true) {
      token = await reply.jwtSign({
        _id: possibleUser._id,
        forename: possibleUser.forename,
        surname: possibleUser.surname,
      });
    } else {
      var error = new Error('Password is incorrect');
      error.statusCode = 401;
      throw error;
    }
    if (ENV === 'production') {
      return reply
        .setCookie('token', token, {
          secure: true,
          //Expiration date of 1 hour
          maxAge: 60 * 60 * 1,
          domain: 'fatstonks.org',
          path: '/',
          httpOnly: true,
          sameSite: 'none',
        })
        .code(200)
        .send({ url: '/user/portfolio' });
    } else {
      return reply
        .setCookie('token', token, {
          //Expiration date of 1 hour
          maxAge: 60 * 60 * 1,
          domain: 'localhost',
          path: '/',
          httpOnly: true,
          sameSite: true,
        })
        .code(200)
        .send({ url: '/user/portfolio' });
    }
  } catch (err) {
    throw boom.boomify(err);
  }
};

/*****************************************/
/****** Authenticated Controllers *******/
/***************************************/
exports.updateUser = async (req, reply) => {
  try {
    const decodedClientToken = await req.jwtVerify();
    const userInfo = { ...decodedClientToken };
    const id = userInfo._id;
    const user = req.body;
    const { ...updateData } = user;
    if (updateData.password) {
      var salt = bcrypt.genSaltSync(10);
      updateData.password = bcrypt.hashSync(user.password, salt);
    }
    var date = new Date();
    var todaysDateLocale = date.toLocaleString('se-SE');
    updateData.registration_date = todaysDateLocale;
    const update = await User.findByIdAndUpdate(id, updateData, {
      new: true,
    });
    if (!update) {
      var error = new Error('There an error updating the user information');
      error.statusCode = 404;
      throw error;
    }
    token = await reply.jwtSign({
      _id: update._id,
      forename: update.forename,
      surname: update.surname,
    });
    if (ENV === 'production') {
      return reply
        .clearCookie('token', { path: '/' })
        .setCookie('token', token, {
          secure: true,
          //Expiration date of 1 hour
          maxAge: 60 * 60 * 1,
          domain: 'fatstonks.org',
          path: '/',
          httpOnly: true,
          sameSite: 'none',
        })
        .code(200)
        .send('User information updated');
    } else {
      return reply
        .clearCookie('token', { path: '/' })
        .setCookie('token', token, {
          //Expiration date of 1 hour
          maxAge: 60 * 60 * 1,
          domain: 'localhost',
          path: '/',
          httpOnly: true,
          sameSite: true,
        })
        .code(200)
        .send('User information updated');
    }
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.logoutUser = async (req, reply) => {
  try {
    if (ENV === 'production') {
      await req.jwtVerify();
      return reply
        .clearCookie('token', { path: '/', domain: 'fatstonks.org' })
        .code(200)
        .send({ url: '/' });
    } else {
      await req.jwtVerify();
      return reply
        .clearCookie('token', { path: '/' })
        .code(200)
        .send({ url: '/' });
    }
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.deleteUser = async (req, reply) => {
  try {
    const decodedClientToken = await req.jwtVerify();
    const userInfo = { ...decodedClientToken };
    const id = userInfo._id;
    const user = await User.findByIdAndRemove(id);
    if (!user) {
      var error = new Error('There was no existing account with this userID');
      error.statusCode = 404;
      throw error;
    }
    return user;
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.findSingleUser = async (req, reply) => {
  try {
    const decodedClientToken = await req.jwtVerify();
    const userInfo = { ...decodedClientToken };
    const id = userInfo._id;
    const user = await User.findById(id);
    if (!user) {
      var error = new Error('There was no existing account with this userID');
      error.statusCode = 404;
      throw error;
    }
    user.password = 'hidden';
    return user;
  } catch (err) {
    throw boom.boomify(err);
  }
};

exports.authenticateUser = async (req, reply) => {
  try {
    const valid = await req.jwtVerify();
    if (valid) {
      return await reply.send(true);
    }
  } catch (err) {
    return await reply.code(401).send(false);
  }
};
