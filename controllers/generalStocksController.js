const boom = require('@hapi/boom');
const Stock = require('../models/stock');

/***********************************/
/****** No Auth Controllers *******/
/*********************************/
exports.getDefaultStockList = async (req, reply) => {
  try {
    const stocks = await Stock.find();
    return stocks;
  } catch (err) {
    throw boom.boomify(err);
  }
};
